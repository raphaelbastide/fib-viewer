$('.colorpicker').minicolors({
    changeDelay: 100,
    position: 'bottom right',
    defaultValue: '#FF6347',
    change: function(value, opacity) {
        $('body').css('backgroundColor',value);
        $('.txtcolor').css('color',value);
    }
});

$('.miniBtn').click(function(){
  $('body').toggleClass('mini');
})
