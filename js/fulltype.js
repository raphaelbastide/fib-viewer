var letterSpeed=70;
var cycleSpeed=8000;
var lines=["Manifeste","Saga Norén, Lanskrim, Malmö","Backdoor","Gordon Zola","Evan Roth","Travail, Famille, Patrick","Panama", "Stéphane 007", "Morty", "Métal rose", "L’ I.A. de 2017", "Groooove", "rimshot", "claude@wetpanties.org"]
var cols=["rgb(240, 142, 129)", "rgb(145, 224, 132)", "rgb(236, 186, 215)", "rgb(170, 176, 194)", "rgb(96, 250, 204)", "rgb(249, 252, 145)", "rgb(181, 204, 191)", "rgb(187, 187, 187)"]
var fonts = $('.font');
var fontlist = [];
fonts.each(function(){
  fontlist.push($(this).attr('data-name'));
})

var write = function (target, message, index, interval) {
  if (index < message.length) {
    $(target).append(message[index++]);
    setTimeout(function(){
      write(target, message, index, interval);
    }, interval);
  }
}

var randLine = lines[Math.floor(Math.random()*lines.length)];

function changeCol(){
  var randCol = cols[Math.floor(Math.random()*cols.length)];
  $('main').css({background:randCol});
}
function changeFont(font){
  $('main').css({fontFamily:font});

}

$('.font').click(function(){
  var name = $(this).attr('data-name');
  var randCol = cols[Math.floor(Math.random()*cols.length)];
  changeFont(name);
})

function showText(index) {
  $('main').html("");
  var randFont = fontlist[Math.floor(Math.random()*fontlist.length)];
  if (index < lines.length) {
    write("main",lines[index],0,letterSpeed);
    setTimeout(function() { showText(index+1); }, cycleSpeed);
    changeCol();
    changeFont(randFont)
  }else {
    setTimeout(function() { showText(0); }, cycleSpeed);
    changeFont(randFont)
    changeCol();
  }
}
showText(0);


$('.colorpicker').minicolors({
    changeDelay: 100,
    position: 'bottom right',
    defaultValue: '#FF6347',
    change: function(value, opacity) {
        $('body').css('backgroundColor',value);
        $('.txtcolor').css('color',value);
    }
});
