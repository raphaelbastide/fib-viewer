# Foundry In a Box (Viewer)

1. Place, or clone a copy of [Fib](https://gitlab.com/foundry-in-a-box/fib) at the root of this directory in a `fib/` folder.
2. Generate font packages (see Fib documentation)
3. Convert the ufo font source(s) into at least a `.woff` file (for Firefox) 
4. Start an apache server and open index.php
