<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Foundry in a box Viewer</title>
    <link rel="stylesheet" href="font/stylesheet.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/minicolors.css">
    <link rel="stylesheet" href="css/fulltype.css">
  </head>
  <body>

  <div class="list">
  <?php
  require_once "functions.php";

  $directory = "fib/";
  if (!is_dir($directory)):
    echo "Directory fib/ not found";
  else:

  $projects = rglob($directory."{*.json}", GLOB_BRACE);
  $projectNbr = 0;

  // Sort projects, most recent first
  //usort($projects, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

  foreach ($projects as $project) {
    $projectNbr ++;
    $contents = file_get_contents($project);
    // $contents = utf8_encode($contents);
    $json = json_decode($contents);
    $fontdir = dirname($project);
    $fonts = count($json->resources);
  ?>
    <?php
    foreach ($json->resources as $font) {
      $fontpsname = $font->postscript_name;
      $woff = $fontdir."/".$fontpsname.".woff";
      $ttf = $fontdir."/".$fontpsname.".ttf";
      if (file_exists($woff)):
        echo "<style>";
        echo "
      @font-face {
          font-family: '{$fontpsname}';
          src: url('{}.eot');
          src: url('{}.eot?#iefix') format('embedded-opentype'),
               url('{$woff}') format('woff'),
               url('{$ttf}') format('truetype'),
               url('{}.svg') format('svg');
          font-weight: normal;
          font-style: normal;
      }";
      echo "</style>";
      echo "<span class='font' data-name='$fontpsname' style='font-family:{$fontpsname};'>{$font->full_name}</span>";
      endif;
    }
    ?>
  <?php }
  endif;
   ?>
  </div>
   <main></main>

  </body>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/minicolors.js"></script>
  <script src="js/fulltype.js"></script>
</html>
