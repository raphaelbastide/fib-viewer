<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Foundry in a box Viewer</title>
    <link rel="stylesheet" href="font/stylesheet.css">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/minicolors.css">
    <link rel="stylesheet" href="css/main.css">
  </head>
  <body>
    <header>
      <h1>Foundry in a <span class="box txtcolor">box</span></h1>
      <h2>Viewer</h2>
      <nav>
        <li><span><button class="miniBtn txtcolor">↕</button></span></li>
        <li><span class="colorpicker"></span></li>
        <li><a class="txtcolor" href="https://gitlab.com/foundry-in-a-box/fib">About</a></li>
      </nav>
    </header>
  <?php
  require_once "functions.php";

  $directory = "fib/";
  if (!is_dir($directory)):
    echo "Directory fib/ not found";
  else:

  $projects = rglob($directory."{*.json}", GLOB_BRACE);
  $projectNbr = 0;

  // Sort projects, most recent first
  //usort($projects, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

  foreach ($projects as $project) {
    $projectNbr ++;
    $contents = file_get_contents($project);
    // $contents = utf8_encode($contents);
    $json = json_decode($contents);
    $fontdir = dirname($project);
    $fonts = count($json->resources);
  ?>
  <section>
    <header>
      <p><?php echo $json->family_name; ?></p>
      <?php if ($json->designer): ?>
        <p>Designer: <?php echo "<a href='$json->designer_url'>$json->designer</a>" ?> </p>
      <?php endif; ?>
      <p>Contains <?php echo "$fonts" ?> font(s) </p>
      <p><a href="<?php echo $project ?>">Edit</a></p>
    </header>
    <main>
        <?php
        foreach ($json->resources as $font) {
          $fontpsname = $font->postscript_name;
          $woff = $fontdir."/".$fontpsname.".woff";
          $ttf = $fontdir."/".$fontpsname.".ttf";
          echo "<div class='font'>";
          echo "<ul class='meta txtcolor'>";
          echo "<li>Name: ".$font->full_name."</li>";
          echo "<li>Weight: ".$font->weight."</li>";
          if ($font->style) {
            echo "<li>Style: ".$font->style."</li>";
          }
          if ($font->italic) {
            echo "<li>Italic<li>";
          }
          echo "<li>version: ".$font->version."</li>";
          echo "</ul>";

          if (file_exists($woff)):
            echo "<style>";
            echo "
          @font-face {
              font-family: '{$fontpsname}';
              src: url('{}.eot');
              src: url('{}.eot?#iefix') format('embedded-opentype'),
                   url('{$woff}') format('woff'),
                   url('{$ttf}') format('truetype'),
                   url('{}.svg') format('svg');
              font-weight: normal;
              font-style: normal;
          }";
          echo "</style>";
          echo "<input class='tester txtcolor' style='font-family:\"{$fontpsname}\";' type=text spellcheck='false' value='Test me!'>";

          endif;
          echo "</div>";
        }
        ?>
      </ul>
    </main>
  </section>
  <?php }
  endif;
   ?>
  <footer></footer>
  </body>
  <script src="js/jquery-1.11.3.min.js"></script>
  <script src="js/minicolors.js"></script>
  <script src="js/main.js"></script>
</html>
